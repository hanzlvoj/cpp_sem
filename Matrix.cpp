
#include <iostream>
#include <cmath>
#include <utility>
#include <fstream>
#include "Matrix.h"


Matrix::Matrix(int rows, int cols) : rows(rows), cols(cols) {
    data.resize(rows * cols);
}

Matrix::Matrix(int rows, int cols, std::vector<double> data): rows(rows), cols(cols), data(std::move(data)) {

}

Matrix::Matrix(const Matrix &other): rows(other.rows), cols(other.cols), data(other.data) {

}

Matrix Matrix::create_identity_matrix(int size){
    Matrix res(size, size);

    for (int i = 0; i < res.get_rows(); i++) {
        for (int j = 0; j < res.get_cols(); j++) {
            if (i == j) {
                res(i,j) = 1;
            } else {
                res(i,j) = 0;
            }
        }
    }
    return res;
}

double &Matrix::operator()(int row, int col) {
    return data[row * cols + col];
}

const double &Matrix::operator()(int row, int col) const {
    return data[row * cols + col];
}

int Matrix::get_rows() const {
    return rows;
}

int Matrix::get_cols() const {
    return cols;
}

void Matrix::swap_rows(int i, int j) {
    for (int k = 0; k < cols; k++) {
        std::swap(data[i * cols + k], data[j * cols + k]);
    }
}

void Matrix::divide_row(int i, double k) {
    for (int j = 0; j < cols; j++) {
        data[i * cols + j] /= k;
    }
}

void Matrix::add_rows(int i, int j, double k) {
    for (int l = 0; l < cols; l++) {
        data[i * cols + l] += data[j * cols + l] * k;
    }
}

int Matrix::find_pivot(int row) {
    int pivot = row;
    for (int j = row + 1; j < rows; j++) {
        if (std::fabs((*this)(j, row)) > std::fabs((*this)(pivot, row))) {
            pivot = j;
        }
    }
    return pivot;
}

void Matrix::under_diagonal_elim(Matrix &I, int row) {
    for (int j = row + 1; j < rows; j++) {
        double mul = -(*this)(j, row) / (*this)(row, row);
        add_rows(j, row, mul);
        I.add_rows(j, row, mul);
    }
}

void Matrix::above_diagonal_elim(Matrix &I, int row) {
    for (int j = row - 1; j >= 0; j--) {
        double mul = -(*this)(j, row) / (*this)(row, row);
        add_rows(j, row, mul);
        I.add_rows(j, row, mul);
    }
}

void Matrix::normalize_pivot_row(Matrix &I, int row) {
    double div = (*this)(row, row);
    if (fabs(div) < 1e-9) {
        throw std::invalid_argument("Matrix is singular");
    }
    divide_row(row, div);
    I.divide_row(row, div);
}

void Matrix::eliminate_current_variable(Matrix &I, int row) {
    under_diagonal_elim(I, row);
    above_diagonal_elim(I, row);
}

std::pair<Matrix, Matrix> Matrix::inverse() {
    // Check if the matrix is square
    if (rows != cols) {
        throw std::invalid_argument("The matrix is not square");
    }
    int size = rows;


    // Create an identity matrix and a copy of the original matrix
    Matrix I = Matrix::create_identity_matrix(size);
    Matrix B(*this);

    // Perform Gauss-Jordan elimination
    for (int row = 0; row < size; row++) {
        // Find the pivot element
        int pivot = B.find_pivot(row);

        // Swap rows if necessary
        if (pivot != row) {
            B.swap_rows(row, pivot);
            I.swap_rows(row, pivot);
        }

        // Normalise the pivot row
        B.normalize_pivot_row(I, row);

        // Eliminate the current variable from the rows above and below
        B.eliminate_current_variable(I, row);
    }
    if(!B.is_identity()){
        std::cerr << "Incorrect result computed!";
    }
    std::pair<Matrix, Matrix> res (I,B);
    return res;


}


std::ostream &operator<<(std::ostream &os, const Matrix &matrix) {
    for (int i = 0; i < matrix.rows; i++) {
        for (int j = 0; j < matrix.cols; j++) {
            os << matrix(i, j) << " ";
        }
        os << "\n";
    }
    os << "\n";
    return os;
}

void Matrix::print_to_file(const std::string &filename, bool append) {
    std::ofstream file;
    double num;

    if(append) {
        file.open(filename, std::ios::app);
    } else{
        file.open(filename);
    }

    if (!file.is_open()) {
        std::cerr << "Error opening input file!" << std::endl;
    }
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            num = (*this)(i, j);
            if(num == -0){
                num = 0;
            }
            file << num << " ";
        }
        file << "\n";
    }
    file << "\n";
    file.close();
}

bool Matrix::is_identity() {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            if (i == j) {
                if((*this)(i,j) != 1){
                    return false;
                }
            } else {
                if((*this)(i,j) != 0){
                    return false;
                }
            }
        }
    }
    return true;
}


