#include <iostream>
#include <cstring>
#include <chrono>
#include <fstream>
#include "Matrix.h"
#include "Parser.h"


template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

void print_help(){
    std::ifstream file("../resources/help.txt");
    std::string line;
    while (std::getline(file, line)) {
        std::cout << line << std::endl;
    }
    file.close();
}

void run_selected_mode(char** argv, bool benchmark = false){
    std::string input_file_name;
    std::string output_file_name;

    if(benchmark){
        input_file_name = "../resources/benchmark.txt";
        output_file_name = "../results/benchmark_result.txt";
    } else{
        std::string argv2 = argv[2];
        std::string argv4 = argv[4];
        input_file_name = "../resources/" + argv2 + ".txt";
        output_file_name = "../results/" + argv4 + ".txt";
    }

    auto start = std::chrono::high_resolution_clock::now();
    std::cout << "Calculation started.\n";

    Matrix A = Parser::parse_matrix(input_file_name);

    std::pair<Matrix, Matrix> result = A.inverse();

    result.first.print_to_file(output_file_name);
    result.second.print_to_file(output_file_name, true);

    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Calculation completed in " << to_ms(end - start).count() << " ms.\n";

}


int main(int argc, char** argv)
{
    if(argc == 2 && strcmp(argv[1], "--help")==0) {
        print_help();

    } else if(argc == 2 && strcmp(argv[1], "-b")==0) {
        run_selected_mode(argv, true);

    } else if(argc == 5 && strcmp(argv[1], "-i")==0 && strcmp(argv[3], "-o")==0) {
        run_selected_mode(argv);

    } else{
        std::cerr << "Invalid program arguments!";
        return -1;
    }

    return 0;
}








