//
// Created by vojtano on 2.1.23.
//

#ifndef CPP_SEM_MATRIX_H
#define CPP_SEM_MATRIX_H


#include <vector>
#include <ostream>

class Matrix {

    public:
        Matrix(int rows, int cols);

        Matrix(int rows, int cols, std::vector<double> data);

        Matrix(const Matrix& other);

        static Matrix create_identity_matrix(int size);

        double& operator()(int row, int col);

        const double& operator()(int row, int col) const;

        int get_rows() const;
        int get_cols() const;

        bool is_identity();


    std::pair<Matrix, Matrix> inverse();

    void print_to_file(const std::string &filename, bool append = false);

    friend std::ostream &operator<<(std::ostream &os, const Matrix &matrix);

    private:
        int rows;
        int cols;
        std::vector<double> data;

        void swap_rows(int i, int j);

        void divide_row(int i, double k);

        void add_rows(int i, int j, double k);

        // Function to find the pivot element
        int find_pivot(int row);

        void normalize_pivot_row(Matrix& I, int row);

        void eliminate_current_variable( Matrix& I, int row);

        // Function to eliminate the current variable from the rows below
        void under_diagonal_elim(Matrix& I, int row);

        // Function to eliminate the current variable from the rows above
        void above_diagonal_elim(Matrix& I, int row);


};


#endif //CPP_SEM_MATRIX_H
