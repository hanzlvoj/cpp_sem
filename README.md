# CPP_SEM



## Měření

Měření proběhlo vůči kódu v commitu 1b89ec31, na 6 jádrovém Ryzen 5 3600 MHz.
Výpočet 1000x1000 matice společně s parsováním a ukládáním do souboru zabere 10600 - 11000 ms.

## Inverse Matrix Calculator

Výpočet inverzní matice je docílen pomocí Gauss-Jordanovy metody.

## Implementace

Gauss-Jordanova metoda je prováděna v těchto krocích:

    1.  Vytvoření kopie z původní matice a jednotkové matice.
    2.  Normalizace řádku s pivotem pomocí dělení celého řádku pivotním prvkem.
    3.  Eliminace aktuální hodnoty z řádku pomocí přičítání adekvátně vynásobeného pivotního řádku.
    4.  Opakování kroků 2 a 3 pro všechny řádky matice.

