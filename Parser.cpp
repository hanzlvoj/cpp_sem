
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include "Parser.h"



Matrix Parser::parse_matrix(const std::string &filename) {
    std::stringstream sstream_dim;
    std::stringstream sstream_vals;

    std::string str = file_to_str(filename);

    sstream_dim.str(str);
    sstream_vals.str(str);

    std::pair<int, int> dim = parse_matrix_dim(sstream_dim);

    std::vector<double> data = parse_matrix_vals(sstream_vals, dim);

    return Matrix(dim.first, dim.second, data);
}



std::pair<int, int> Parser::parse_matrix_dim(std::istream& in){
    std::pair<int, int> res;
    res.first = 0;
    res.second = 0;

    std::string line;
    int rows = 0;
    int cols;
    int temp_cols = 0;
    int prev_cols = 0;
    bool first_iter = true;
    while(std::getline(in, line)){
        if(line != "") {
            rows++;

            std::stringstream sstream(line);
            std::string num;

            temp_cols = 0;
            while (sstream >> num) {
                temp_cols++;
            }
            if (temp_cols != prev_cols && !first_iter) {
                throw std::invalid_argument("received invalid matrix dimensions");
            }
            first_iter = false;
            prev_cols = temp_cols;
        }
    }
    cols = temp_cols;
    res.first = rows;
    res.second = cols;
    return res;
}

std::vector<double> Parser::parse_matrix_vals(std::istream& in, const std::pair<size_t, size_t>& m_size){

    std::string line;
    std::vector<double> nums;
    size_t col_count;

    while(std::getline(in, line)) {

        std::stringstream sstream(line);
        double num;
        std::string str;

        col_count = 0;
        while (sstream >> str) {
            num = std::stod(str);
            nums.push_back(num);
            col_count++;
        }
        if(col_count != m_size.second){
            throw std::invalid_argument("received invalid matrix dimensions");
        }
    }
    return nums;
}

std::string Parser::file_to_str(const std::string &filename){
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Error opening input file" << std::endl;

    }
    std::string line;
    std::string str;
    std::stringstream ret;

    while (std::getline(file, line)) {
        str += line;
        str += '\n';
    }

    return str;

}






