

#ifndef CPP_SEM_PARSER_H
#define CPP_SEM_PARSER_H


#include "Matrix.h"

class Parser {
    private:

         static std::vector<double> parse_matrix_vals(std::istream& in, const std::pair<size_t, size_t>& m_size);

         static std::pair<int, int> parse_matrix_dim(std::istream& in);

         static std::string file_to_str(const std::string &filename);

    public:
        static Matrix parse_matrix(const std::string &filename);
};


#endif //CPP_SEM_PARSER_H
